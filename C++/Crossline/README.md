# Crossline

Crossline是一个用以美化命令行程序交互的扩展库。它支持自定义快捷键，高级搜索，输入自动补全，换页，光标APIs，颜色APIs，内嵌帮助文档等等。并且它是一个跨平台的库，可以支持Windows、Linux、MacOS之间零修改的使用。

与GNU Readline相比，Crossline的功能还有所欠缺，例如它不支持对于路径的自动补全，但这些功能是有可能由你自己扩展的。总的来说，你应该在以下情况使用该库：

1. 当你需要一个跨平台的readline
2. 当你需要一个简单易上手，但又足够丰富的readline
3. 当你需要一个易于自定义扩展的readline
4. 当你需要一个足够小的readline

详细的API说明请看README_en，或者参考example中给出的`example_sql.c`，它完整的涉及了所有功能。如果仅仅需要**自动补全**、**颜色美化**等基本功能，可以参考我写的`example_by_morphlng.cpp`