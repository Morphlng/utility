<p align="center">
	<a target="_blank" href="http://getrobot.net">
		<img src="http://getrobot.net/common/gh-header.png" alt="ROBOT" />
	</a>
</p>

<h1 align="center">Native System Automation</h1>

<p align="center">
	<a target="_blank" href="http://getrobot.net">
		<img src="https://img.shields.io/badge/Built_with-ROBOT-C86414.svg?style=flat-square" alt="Built with Robot" />
	</a>
	<img src="https://img.shields.io/badge/Version-2.0.0-C86414.svg?style=flat-square" alt="Version 2.0.0" />
	<a target="_blank" href="http://getrobot.net/api/global.html">
		<img src="https://img.shields.io/badge/Docs-API-C86414.svg?style=flat-square" alt="Docs API" />
	</a>
	<a target="_blank" href="https://opensource.org/licenses/Zlib">
		<img src="https://img.shields.io/badge/License-ZLIB-C86414.svg?style=flat-square" alt="ZLIB Licensed" />
	</a>
</p>

<h5 align="center">
	<a target="_blank" href="http://getrobot.net/docs/usage.html">
		GET STARTED
	</a>
	&nbsp;|&nbsp;
	<a target="_blank" href="http://getrobot.net/docs/about.html">
		DOCUMENTATION
	</a>
	&nbsp;|&nbsp;
	<a target="_blank" href="http://getrobot.net/api/global.html">
		API
	</a>
	&nbsp;|&nbsp;
	<a target="_blank" href="http://getrobot.net/docs/philosophy.html">
		COMMUNITY
	</a>
</h5>

## 简介

Robot是一个使用C++编写的，跨平台、自动化操作封装库。它封装了绝大多数操作系统（包括Windows、Linux和MacOS）中用户交互的方法，并提供了一致、可靠的API接口。Robot的功能从模拟基本的键鼠输入，到复杂的进程管控应有尽有，更多详细信息以及接口的使用方法，请见[官网](http://getrobot.net)

<p align="justify">
	Introducing Robot for C++, a library aimed at facilitating the development of system automation software for the purposes of test automation, self-running demos, and other applications. The library works by abstracting away all platform-specific differences into a single, robust API compatible with most desktop operating systems. Functionality ranges from basic keyboard and mouse automation to advanced process manipulation capabilities. It has everything you need to take full control of your system. Visit the <a target="_blank" href="http://getrobot.net">Homepage</a> for more information.
</p>
