# gitdir

- Minimal and colorful output 🌈 <img src="https://user-images.githubusercontent.com/27065646/71288165-9914bc80-236a-11ea-853b-a97bff999e79.gif" align="right">
- Works on **Linux**, **MacOS**, and **Windows**
- Support recursive downloading
<br>
<br>
<br>
<br>
<br><br>
<br><br>
<br>

## 介绍

就像上面原始Readme中提到的，这是一个跨平台(Python)的github仓库子文件下载工具。

对于一些很大的仓库，或者课程仓库，往往我们只需要其中一个文件夹，甚至一个文件，整仓克隆没有必要，单个文件下载速度太慢，这时候gitdir就可以帮助你。

## 安装

```bash
$ pip3 install --user gitdir
```

在Windows下安装后可能会提示你将gitdir.exe所在位置加入环境变量Path，推荐照做，否则调用不那么方便。

## Usage

```
usage: gitdir [-h] [--output_dir OUTPUT_DIR] [--flatten] urls [urls ...]

Download directories/folders from GitHub

positional arguments:
  urls                  List of Github directories to download.

optional arguments:
  -h, --help            show this help message and exit
  --output_dir OUTPUT_DIR, -d OUTPUT_DIR
                        All directories will be downloaded to the specified
                        directory.
  --flatten, -f         Flatten directory structures. Do not create extra
                        directory and download found files to output
                        directory. (default to current directory if not
                        specified)
```

## Packge Entry

You can use `python -m gitdir` / `python3 -m gitdir` in case the short command does not work.

**Exiting**

To exit the program, just press ```CTRL+C```.

## License
MIT License

Copyright (c) 2019 Siddharth Dushantha
