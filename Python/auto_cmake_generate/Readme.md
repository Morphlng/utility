# Auto CMakeLists Generate

该脚本可以搭配Clion使用，为一个多文件夹、多源文件项目自动搜索所有cpp文件，并生成CMakeLists.txt

## 使用

1. 命令行使用`python autogenerate.py`运行脚本
2. 输入项目名称，该名称应为根目录名
3. 输入源文件所在地址（相对根目录）
4. 输入main函数所在文件路径（相对根目录）
5. 等待完成

## 错误

1. 在Windows下因为路径分割符为'\\'而导致`str.split('/')`错误，已被我修复