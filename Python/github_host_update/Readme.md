# Decription

在腾讯云或者阿里云这样的环境中，一般不会开启代理，这导致从github拉取代码十分缓慢。

通过修改hosts可以有一定缓解，故写下该python脚本去获取延迟最低的ip地址。

## Usage

1. 确保系统拥有Python3环境，安装好requests库
2. 确保你的hosts文件符合如下格式

```hosts
xxxxxx  xxxx.xxxx.xxxx

# GitHub Host Start
{content}
# GitHub Host End
```

即存在github地址段的标识符，否则其他hosts设置将无法保留

3. 执行脚本：

```bash
python github_host_update.py
```

这将在同目录下生成一个全新的hosts文件`new_hosts`

4. 确保无误后，使用新hosts文件替换原始hosts

```bash
cp /etc/hosts ~/hosts.bak
sudo mv newhosts /etc/hosts
```