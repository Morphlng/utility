# Utility

## 介绍

1. 该仓库用于收录我在开发过程中查到的，使用过的各种实用代码，方便后续查找使用。

2. 该仓库使用GPL-3.0 License，但使用代码时应以其给出的License为凭证。

3. 该仓库不收录大型扩展仓库（如boost），一般情况下收录的为开箱即用的Headers only代码

## 参与贡献

1.  Fork 本仓库
2.  新建 Pull Request
3.  发起Issue